import os
import pickle

from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request

def get_google_credentials(credentials_path, pickle_path, scope_list):
    creds = None

    if os.path.exists(pickle_path):
        with open(pickle_path, 'rb') as token:
            creds = pickle.load(token)

    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
            credentials_path, scope_list)
            creds = flow.run_local_server(port=0)

            with open(pickle_path, 'wb') as token:
                pickle.dump(creds, token)
    
    return creds
        