from time import time, sleep
from threading import Thread, Lock

from Prototypes import Reader_prototype

from selenium import webdriver

class Facebook_reader(Reader_prototype, Thread):
    def __init__(self, video_url_list, fb_email, fb_password, refresh_interval, messages_container):
        self.prev_comments_list = []

        self.messages_container = messages_container

        options = webdriver.FirefoxOptions()
        self.driver = webdriver.Firefox(options=options)

        self.driver.get("https://www.facebook.com/login.php")

        sleep(2)

        submit_cookies_button = self.driver.find_element_by_xpath("//*[text() = 'Accept All']")
        submit_cookies_button.click()

        email_input_field = self.driver.find_element_by_id("email")
        password_input_field = self.driver.find_element_by_id("pass")

        email_input_field.send_keys(fb_email)
        password_input_field.send_keys(fb_password)

        login_button = self.driver.find_element_by_id("loginbutton")
        login_button.click()

        for url in video_url_list:
            self.driver.execute_script('''window.open("{0}","_blank");'''.format(url))
            self.driver.get(url)
            sleep(2)

        sleep(30)

        self.video_list = self.driver.window_handles[1:]

        self.refresh_interval = refresh_interval

        self.run_thread = bool(False)
        Thread.__init__(self)
    
    def read_new_comments(self):
        all_comments_list = []

        for video in self.video_list:
                self.driver.switch_to_window(video)
                print(self.driver.title)

                try:
                    comments_list = self.driver.find_elements_by_xpath("//*[@class='tw6a2znq sj5x9vvc d1544ag0 cxgpxx05']")

                    for comment in comments_list:
                        name_elem = comment.find_element_by_xpath(".//span[@class='d2edcug0 hpfvmrgz qv66sw1b c1et5uql gk29lw5a a8c37x1j keod5gw0 nxhoafnm aigsh9s9 tia6h79c fe6kdd0r mau55g9w c8b282yb iv3no6db e9vueds3 j5wam9gi lrazzd5p oo9gr5id']")

                        try:
                            text_elem = comment.find_element_by_xpath(".//div[@dir='auto']")
                            
                            emoji_list = text_elem.find_elements_by_xpath(".//span[@class='pq6dq46d tbxw36s4 knj5qynh kvgmc6g5 ditlmg2l oygrvhab nvdbi5me sf5mxxl7 gl3lb2sf hhz5lgdu']")

                            if len(emoji_list) != 0:
                                text = text_elem.get_attribute("innerHTML")
                                substitute_list = []

                                for child_node in text_elem.find_elements_by_xpath("./*"):
                                    if child_node.tag_name == "span":
                                        emoji_node = child_node.find_element_by_tag_name("img")
                                        emoji_dict = {
                                            "node_html" : child_node.get_attribute("outerHTML"),
                                            "node_content" : emoji_node.get_attribute("alt")
                                        }

                                        substitute_list.append(emoji_dict)
                                    else:
                                        link_dict = {
                                            "node_html" : child_node.get_attribute("outerHTML"),
                                            "node_content" : child_node.text
                                        }

                                        substitute_list.append(link_dict)
                                
                                for eachelement in substitute_list:
                                    # print("Source_HTML:\n{0}".format(eachelement["node_html"]))
                                    # print("Substitute_content: {0}".format(eachelement["node_content"]))
                                    # print("Text - no substitution:\n{0}".format(text))
                                    text = text.replace(eachelement["node_html"], eachelement["node_content"], 1)
                                    # print("Text - after substitution:\n{0}".format(text))

                            
                            else:
                                text = text_elem.text

                        except Exception as e:
                            print(e)
                            text = ""


                        if len(text) != 0:
                            comment_dict = {
                                "name" : name_elem.text,
                                "text" : text,
                                "source" : "Facebook"
                            }

                            if comment_dict not in self.prev_comments_list:
                                self.prev_comments_list.append(comment_dict)
                                all_comments_list.append(comment_dict)

                except Exception as e:
                    print(e)

        self.debug_print(all_comments_list)
        return all_comments_list
        
    def debug_print(self, comments_list):
        for comment in comments_list:
            print("Comment:\nAuthor: {0}\nText:\n{1}\r\n".format(comment["name"], comment["text"]))

    def run(self):
        self.run_thread = True
        prev_reading = time()
        print("Started FB thread")

        while True:
            sleep(0.1)
            current_reading = time()

            if self.run_thread:
                if current_reading - prev_reading >= self.refresh_interval:
                    print("Reading FB")
                    comments_list = self.read_new_comments()
                    self.messages_container.extend(comments_list)
                    prev_reading = current_reading
            else:
                print("Stopping FB thread")
                break
    
    def stop(self):
        self.run_thread = False
        

