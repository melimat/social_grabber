from Sheets_writer import Sheets_writer
from You_Tube_reader import You_Tube_reader
from Facebook_reader import Facebook_reader
import time

from get_google_credentials import get_google_credentials
from You_Tube_reader import You_Tube_reader

scopes = [
    "https://www.googleapis.com/auth/spreadsheets",
    "https://www.googleapis.com/auth/youtubepartner",
    "https://www.googleapis.com/auth/youtube.force-ssl"
]

video_url_list = [
    "https://www.facebook.com/101301361864658/videos/264133075049029/",
    "https://www.facebook.com/104308754780185/videos/827899871276360/"
]
fb_email = ""
fb_password = ""

messages_container = []

yt_credentials = get_google_credentials("credentials.json", "yt_token.pickle", scopes)
sheets_credentials = get_google_credentials("credentials.json", "sheets_token.pickle", scopes)
yt_reader = You_Tube_reader(yt_credentials, "4iXlA8TfEAg", 30, messages_container)
sheets_writer = Sheets_writer(sheets_credentials, "12QqtvDFFPIblyAWxTaiS9V8oyzyHNBKV2oHtJiYRXWs", messages_container, 45)
fb_reader = Facebook_reader(video_url_list, fb_email, fb_password, 30, messages_container)


yt_reader.start()
fb_reader.start()
sheets_writer.start()
while True:
    print(messages_container)
    time.sleep(30)