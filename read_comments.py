import time

from get_google_credentials import get_google_credentials

from Facebook_reader import Facebook_reader
from You_Tube_reader import You_Tube_reader
from Sheets_writer import Sheets_writer

def read_comments():
    video_url_list = [
        "https://www.facebook.com/Smookingbones/videos/216213730094588"
    ]
    fb_email = ""
    password = ""

    scope_list = [
        "https://www.googleapis.com/auth/spreadsheets",
        "https://www.googleapis.com/auth/youtubepartner",
        "https://www.googleapis.com/auth/youtube.force-ssl"
        ]
    yt_credentials = get_google_credentials("credentials.json", "yt_token.pickle", scope_list)
    sheets_credentials = get_google_credentials("credentials.json",  "sheets_token.pickle", scope_list)


    sheet_id = "11sWYwIheglfcEblZXGnrkoR1IC5hpxfXz3RmSr_PjbA"
    sheets_writer = Sheets_writer(sheets_credentials, sheet_id)
    fb_reader = Facebook_reader(video_url_list, fb_email, password)
    yt_reader = You_Tube_reader(yt_credentials, "W1y8XkNKXW8")

    while True:
        fb_comment_list = fb_reader.read_new_comments()
        yt_comments_list = yt_reader.read_new_comments()
        sheets_writer.write_comments(fb_comment_list + yt_comments_list)
        time.sleep(30)


if __name__ == "__main__":
    read_comments()
