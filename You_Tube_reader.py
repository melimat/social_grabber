from time import time, sleep
from threading import Thread, Lock

from Prototypes import Reader_prototype

from googleapiclient.discovery import build

class You_Tube_reader(Reader_prototype, Thread):
    def __init__(self, credentials, live_broadcast_id, refresh_interval, messages_container):
        self.youtube_service = build("youtube", "v3", credentials=credentials)

        live_chat_id_request = self.youtube_service.liveBroadcasts().list(
            id = live_broadcast_id,
            part = "snippet"
        )

        live_chat_id_response = live_chat_id_request.execute()
        self.live_chat_id = live_chat_id_response["items"][0]["snippet"]["liveChatId"]

        self.next_page_token = None
        self.refresh_interval = refresh_interval

        self.messages_container = messages_container

        self.run_thread = float(False)
        Thread.__init__(self)


    def read_new_comments(self):
        comments_list = []

        args = {
            "liveChatId" : self.live_chat_id,
            "part" : "authorDetails,snippet"
        }

        if self.next_page_token:
            args["pageToken"] = self.next_page_token

        live_chat_messages_request = self.youtube_service.liveChatMessages().list(**args)

        try:
            live_chat_messages_response = live_chat_messages_request.execute()

            self.next_page_token = live_chat_messages_response["nextPageToken"]

            for item in live_chat_messages_response["items"]:
                name = item["authorDetails"]["displayName"]
                text = item["snippet"]["displayMessage"]

                comment_dict = {
                    "name" : name,
                    "text" : text,
                    "source" : "YouTube"
                }

                comments_list.append(comment_dict)
    
        except Exception as e:
            print(e)

        return comments_list
    
    def run(self):
        self.run_thread = True
        prev_reading = time()
        print("Started YT thread")

        while True:
            sleep(0.1)
            current_reading = time()
            if self.run_thread:
                if current_reading - prev_reading >= self.refresh_interval:
                    print("Reading YT")
                    messages_list = self.read_new_comments()
                    self.messages_container.extend(messages_list)
                    prev_reading = current_reading
            else:
                print("Stopping YT thread")
                break
    
    def stop(self):
        self.run_thread = False




        
        

