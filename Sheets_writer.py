from threading import Thread, current_thread
from time import time, sleep

from Prototypes import Writer_prototype

from googleapiclient.discovery import build

class Sheets_writer(Writer_prototype, Thread):
    def __init__(self, credentials, spreadsheet_id, messages_container, writing_interval):
        self.service = build('sheets', 'v4', credentials=credentials)
        self.sheet = self.service.spreadsheets()

        self.messages_container = messages_container

        self.spreadsheet_id = spreadsheet_id

        self.run_thread = bool(False)
        self.writing_interval = writing_interval

        Thread.__init__(self)
    
    def write_comments(self):
        cells_list = []
        print(len(self.messages_container))
        while len(self.messages_container) != 0:
            current_message = self.messages_container.pop()
            cells_list.append([current_message["source"], current_message["name"], current_message["text"]])
        
        try:
            cells_list = cells_list[::-1]
            result = self.service.spreadsheets().values().append(
                spreadsheetId=self.spreadsheet_id,
                range = "A1:C1",
                valueInputOption = "RAW",
                body = {
                "values" : cells_list
                }
            ).execute()
        
        except Exception as e:
            print(e)
    
    def run(self):
        self.run_thread = True
        prev_reading = time()
        print("Started Sheets writing thread")

        while True:
            sleep(0.1)
            current_reading = time()

            if self.run_thread:
                if current_reading - prev_reading >= self.writing_interval:
                    print("Writing to GSheets")
                    self.write_comments()
                    prev_reading = current_reading
            
            else:
                if len(self.messages_container != 0):
                    print("Writing last messages from container")
                    self.write_comments()
                
                print(len(self.messages_container))
                print("Stopping writer thread")

                break

    def stop(self):
        self.run_thread = False
                
                
                    

                    

