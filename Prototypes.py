from abc import ABC, abstractmethod

class Reader_prototype(ABC):
    @abstractmethod
    def read_new_comments(self):
        pass
    @abstractmethod
    def run(self):
        pass
    @abstractmethod
    def stop(self):
        pass


class Writer_prototype(ABC):
    @abstractmethod
    def write_comments(self):
        pass
    @abstractmethod
    def run(self):
        pass
    @abstractmethod
    def stop(self):
        pass